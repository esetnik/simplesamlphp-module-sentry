<?php

/**
 * Configuration for the Sentry integration module.
 */

// Please consult https://docs.sentry.io/platforms/php/configuration/options/ for available keys.
$config = [
    'dsn' => '',
    'environment' => '',
    'release' => '',
];
