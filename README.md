# SimpleSAMLphp Sentry intagration

Simple module for SimpleSAMLphp to integrate it with [Sentry](https://sentry.io).

## Installation

```shell
$ composer require kroskolii/simplesamlphp-module-sentry
```

## Configuration

Copy `module_sentry.php` from `config-templates/` directory of this module into `config` directory of your SimpleSAMLphp
installation and fill in or set additional key in `$config` array.
Please consult <https://docs.sentry.io/platforms/php/configuration/options/> for available keys.

**Note** also that `DSN`, `release` and `environment` options can be read from the following environment variables
automatically:

* `SENTRY_DSN`
* `SENTRY_RELEASE`
* `SENTRY_ENVIRONMENT`
 
